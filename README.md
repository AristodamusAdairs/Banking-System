<img align="right" alt="GIF"
src="https://github.com/AristodamusAdairs/GIF-Collection/blob/main/phone-banking-bank.gif" width="400" height="250"/>
<img align="left" alt="GIF"
src="https://github.com/AristodamusAdairs/GIF-Collection/blob/main/tenor.gif" width="200" height="200"/>
# BANKING SYSTEM
ATM Banking System is a banking software application developed in ASP.NET. Automated Teller Machine, abbreviated as ATM, is widely used all over the world to perform various banking operations. With this ATM Banking System software, various tasks done by a typical Bank Management Application can be done such as: adding customer details, viewing customer cash withdrawal/deposit details, manage banking accounts, generating pin, generating receipt upon withdrawal of cash, and many more.
